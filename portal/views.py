# -*- coding: utf-8 -*-
"""
Copyright 2017 Dmitriy Vasilyev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
from __future__ import unicode_literals
from django.conf import settings
from django.http import Http404
from django.shortcuts import render, redirect
from django.core.mail import EmailMessage
from django.core.urlresolvers import reverse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.template.loader import get_template


from datetime import datetime

from .models import Articles, Profile
from .models import LoginForm, ResetPasswordForm, NewPasswordForm, ChangePasswordForm
from .models import RegistrationForm, REGISTRATION_PROFILE_FORMS


def articles(request, article):
    """
    Статьи
    """

    try:
        content = Articles.objects.get(pk=article)
    except Articles.DoesNotExist:
        raise Http404

    return render(request, 'article.html', {
        'title': content.title,
        'text': content.text,
    })


def sign_in(request):
    """
    Страница авторизации
    """

    title = 'Авторизация'

    if request.POST:
        form = LoginForm(request.POST)

        if form.is_valid():
            user = authenticate(
                username=form.cleaned_data['username'],
                password=form.cleaned_data['password'],
            )

            if user is not None:

                if user.is_active:

                    login(request, user)

                    if 'next' in request.GET:
                        return redirect(request.GET.get('next'))
                    else:
                        return redirect('/')

                else:
                    messages.error(request, 'Пользователь неактивен')
            else:
                messages.error(request, 'Неправильное имя пользователя или пароль')

    else:
        form = LoginForm()

    return render(request, 'forms.html', {
        'title': title,
        'forms': (form, ),
        'submit': 'Войти',
        'buttons': (
            {'submit': 'Забыли пароль?', 'url': reverse('reset_password')},
        )
    })


def sign_out(request):
    """
    Выход
    """
    logout(request)
    return redirect(settings.LOGIN_URL)


def registration(request, person):
    """
    Регистрация
    """

    title = 'Регистрация'

    if person not in REGISTRATION_PROFILE_FORMS:
        raise Http404

    if request.POST:

        user_form = RegistrationForm(request.POST, prefix='user')
        profile_form = REGISTRATION_PROFILE_FORMS[person](request.POST, prefix='profile')

        if user_form.is_valid() and profile_form.is_valid():

            profile = profile_form.save(commit=False)
            user = user_form.save(commit=False)

            message = get_template('portal/registration.txt')

            url = ('https://' if request.is_secure() else 'http://') + request.get_host()

            context = {
                'url': url,
                'key': profile.activation_key,
                'login': user.username,
                'password': user.unsafe_password,
            }

            email = EmailMessage(
                subject='%s Регистрация на сайте' % settings.EMAIL_SUBJECT_PREFIX,
                body=message.render(context),
                to=[user.email, ],
            )

            email.send()

            user.save()
            profile.user = user
            profile.save()

            messages.success(request, 'На вашу электронную почту было отправлено сообщение с дальнейшими инструкциями')
            return redirect(settings.LOGIN_URL)

    else:
        user_form = RegistrationForm(prefix='user')
        profile_form = REGISTRATION_PROFILE_FORMS[person](prefix='profile')

    return render(request, 'forms.html', {
        'title': title,
        'forms': (user_form, profile_form,),
        'submit': 'Регистрация'
    })


def activation(request):
    """
    Активация
    """

    if 'key' not in request.GET:
        messages.error(request, 'Не хватает параметров для выполнения запроса')
        return redirect(settings.LOGIN_URL)

    try:
        profile = Profile.objects.select_related().get(
            activation_key=request.GET.get('key'),
            key_expires__gte=datetime.now(),
            user__is_active=False
        )
    except (Profile.DoesNotExist, Profile.MultipleObjectsReturned):
        messages.error(request, 'Пользователь не найден')
        return redirect(settings.LOGIN_URL)

    profile.user.is_active = True
    profile.activation_key = None
    profile.key_expires = None
    profile.user.save()
    profile.save()

    messages.success(request, 'Пользователь активирован, теперь вы можете войти')
    return redirect(settings.LOGIN_URL)


@login_required()
def change_password(request):
    """
    Смена пароля
    """

    title = 'Смена пароля'

    if request.POST:
        form = ChangePasswordForm(data=request.POST, user=request.user)

        if form.is_valid():
            form.save()
            messages.success(request, 'Пароль обновлён')
            logout(request)
            return redirect(settings.LOGIN_URL)

    else:
        form = ChangePasswordForm(user=request.user)

    return render(request, 'forms.html', {
        'title': title,
        'forms': (form, ),
        'submit': 'Изменить'
    })


def reset_password(request):
    """
    Сброс пароля
    """

    title = 'Сброс пароля'

    if request.POST:
        form = ResetPasswordForm(request.POST)

        if form.is_valid():

            result = form.save()

            message = get_template('portal/reset_password.txt')

            url = ('https://' if request.is_secure() else 'http://') + request.get_host()

            context = {
                'url': url,
                'key': result.get('key'),
            }

            email = EmailMessage(
                subject='%s Восстановление пароля' % settings.EMAIL_SUBJECT_PREFIX,
                body=message.render(context),
                to=[result.get('email'), ],
            )

            email.send()

            messages.success(request, 'На вашу электронную почту было отправлено сообщение с дальнейшими инструкциями')
    else:
        form = ResetPasswordForm()

    return render(request, 'forms.html', {
        'title': title,
        'forms': (form, ),
        'submit': 'Отправить'
    })


def reset_password_confirm(request):
    """
    Установка нового пароля
    """

    title = 'Сброс пароля'

    if 'key' not in request.GET:
        raise Http404

    try:
        profile = Profile.objects.get(activation_key=request.GET.get('key'))
    except (Profile.DoesNotExist, Profile.MultipleObjectsReturned):
        raise Http404

    if request.POST:
        form = NewPasswordForm(data=request.POST, user=profile.user)

        if form.is_valid():

            form.save()

            profile.activation_key = None
            profile.key_expires = None
            profile.save()

            messages.success(request, 'Пароль обновлён')
            return redirect(settings.LOGIN_URL)
    else:
        form = NewPasswordForm(user=profile.user)

    return render(request, 'forms.html', {
        'title': title,
        'forms': (form, ),
        'submit': 'Изменить'
    })

