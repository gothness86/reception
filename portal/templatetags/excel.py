# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django import template
from django.utils.html import escape
from django.template.defaultfilters import safe, stringfilter

register = template.Library()


@register.filter
@stringfilter
def excel_text(value):
    """
    Дополнительные преобразования для текста в Excel XML файлах
    """

    result = escape(value).replace("\n", "&#10;")
    return safe(result)

