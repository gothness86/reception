# -*- coding: utf-8 -*-
"""
Copyright 2017 Dmitriy Vasilyev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
from __future__ import unicode_literals
from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser, Group
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible


class User(AbstractUser):
    """
    Расширение модели пользователя
    """

    LABELS = {
        'middle_name': 'Отчество',
    }

    middle_name = models.CharField(
        max_length=30,
        blank=True,
        null=True,
        verbose_name=LABELS['middle_name'])

    @classmethod
    def get_verbose_name(cls, field):
        return cls._meta.get_field(field).verbose_name


@python_2_unicode_compatible
class Profile(models.Model):
    """
    Профиль пользователя
    """

    LABELS = {
        'user':     'Пользователь',
        'type':     'Тип учётной записи',
        'org_name': 'Оф. наим.',
        'inn':      'ИНН',
        'ogrn':     'ОГРН',
    }

    # Типы учётных записей
    ACCOUNT_TYPES = (
        ('individual-person', 'Физическое лицо'),
        ('sole-proprietor', 'Индивидуальный предприниматель'),
        ('legal-person', 'Юридическое лицо'),
    )

    class Meta:
        verbose_name = 'Профиль'
        verbose_name_plural = 'Профиль'

    user = models.OneToOneField(settings.AUTH_USER_MODEL, verbose_name=LABELS['user'])
    type = models.CharField(max_length=20, choices=ACCOUNT_TYPES, verbose_name=LABELS['type'])
    org_name = models.CharField(max_length=255, null=True, blank=True, verbose_name=LABELS['org_name'])
    inn = models.CharField(max_length=12, null=True, blank=True, verbose_name=LABELS['inn'])
    ogrn = models.CharField(max_length=13, null=True, blank=True, verbose_name=LABELS['ogrn'])
    activation_key = models.CharField(max_length=40, null=True, blank=True)
    key_expires = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.user.username


class ProxyGroup(Group):
    """
    Прокси модель группы
    """

    class Meta:
        proxy = True
        verbose_name = _('group')
        verbose_name_plural = _('groups')

