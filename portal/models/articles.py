# -*- coding: utf-8 -*-
"""
Copyright 2017 Dmitriy Vasilyev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
from __future__ import unicode_literals
from django.db import models
from django.utils.encoding import python_2_unicode_compatible


@python_2_unicode_compatible
class Articles(models.Model):
    """
    Статьи
    """

    LABELS = {
        'title':    'Заголовок',
        'text':     'Текст',
    }

    class Meta:
        verbose_name = 'Статьи'
        verbose_name_plural = 'Статьи'

    title = models.CharField(max_length=255, verbose_name=LABELS['title'])
    text = models.TextField(verbose_name=LABELS['text'])
    date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title
