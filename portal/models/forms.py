# -*- coding: utf-8 -*-
"""
Copyright 2017 Dmitriy Vasilyev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
from __future__ import unicode_literals
from django import forms
from django.contrib.auth import get_user_model
from django.utils.crypto import get_random_string
from django.contrib.auth.forms import SetPasswordForm, PasswordChangeForm


from datetime import datetime, timedelta

from .users import User, Profile


class PersonalDataValidator:
    """
    Согласие на обработку персональных данных
    """

    @staticmethod
    def validate(personal_data):
        if not personal_data:
            raise forms.ValidationError('Вы должны согласиться на обработку персональных данных')
        return personal_data


class LoginForm(forms.Form):
    """
    Форма авторизации
    """

    LABELS = {
        'username': 'Имя пользователя',
        'password': 'Пароль'
    }

    username = forms.CharField(
        label=LABELS['username'],
        max_length=255,
        widget=forms.TextInput(),
        required=True,
    )

    password = forms.CharField(
        label=LABELS['password'],
        max_length=255,
        widget=forms.PasswordInput(),
        required=True,
    )


class RegistrationForm(forms.ModelForm):
    """
    Форма регистрации
    """

    LABELS = {
        'first_name':   'Имя',
        'middle_name':  'Отчество',
        'last_name':    'Фамилия',
        'email':        'E-mail',
    }

    HELP_TEXTS = {
        'first_name':   'Введите ваше настоящее имя',
        'email':        'Введите адрес электронной почты',
    }

    first_name = forms.CharField(
        label=LABELS['first_name'],
        help_text=HELP_TEXTS['first_name'],
        max_length=30,
        widget=forms.TextInput(),
        required=True,
    )

    middle_name = forms.CharField(
        label=LABELS['middle_name'],
        max_length=30,
        widget=forms.TextInput(),
        required=True,
    )

    last_name = forms.CharField(
        label=LABELS['last_name'],
        max_length=30,
        widget=forms.TextInput(),
        required=True,
    )

    email = forms.EmailField(
        label=LABELS['email'],
        help_text=HELP_TEXTS['email'],
        widget=forms.EmailInput(),
        required=True
    )

    class Meta:
        model = User
        fields = ['last_name', 'first_name', 'middle_name', 'email', ]

    def clean_email(self):

        email = self.cleaned_data.get('email')

        user = get_user_model()

        if user.objects.filter(email=email).count():
            raise forms.ValidationError('Почтовый адрес уже используется')

        return email

    def save(self, commit=True):

        user = super(RegistrationForm, self).save(commit=False)

        user.is_active = False
        user.username = user.email
        user.unsafe_password = get_random_string(length=8)
        user.set_password(user.unsafe_password)

        if commit:
            user.save()

        return user


class IndividualPersonRegistrationForm(forms.ModelForm):
    """
    Форма для регистрации физических лиц
    """

    LABELS = {
        'personal_data':    'Согласие на обработку персональных данных'
    }

    HELP_TEXTS = {
        'personal_data':    'Отметив галочку вы соглашаетесь на хранение и обработку своих персональных данных',
    }

    personal_data = forms.BooleanField(
        label=LABELS['personal_data'],
        help_text=HELP_TEXTS['personal_data'],
        validators=[PersonalDataValidator.validate, ],
        required=False
    )

    class Meta:
        model = Profile
        fields = ['personal_data', ]

    def save(self, commit=True):
        profile = super(IndividualPersonRegistrationForm, self).save(commit=False)

        profile.type = 'individual-person'
        profile.activation_key = get_random_string(length=40)
        profile.key_expires = datetime.now() + timedelta(days=1)

        if commit:
            profile.save()

        return profile


class SoleProprietorRegistrationForm(forms.ModelForm):
    """
    Форма для регистрации индивидуальных предпринимателей
    """

    LABELS = {
        'org_name':         'Наименование ИП',
        'inn':              'ИНН',
        'personal_data':    'Согласие на обработку персональных данных',
    }

    HELP_TEXTS = {
        'personal_data':    'Отметив галочку вы соглашаетесь на хранение и обработку своих персональных данных',
    }

    org_name = forms.CharField(
        label=LABELS['org_name'],
        max_length=255,
        widget=forms.TextInput(),
        required=True,
    )

    inn = forms.RegexField(
        label=LABELS['inn'],
        min_length=12,
        max_length=12,
        regex=r'^[\d]{12}$',
        widget=forms.TextInput(),
        required=True,
    )

    personal_data = forms.BooleanField(
        label=LABELS['personal_data'],
        help_text=HELP_TEXTS['personal_data'],
        validators=[PersonalDataValidator.validate, ],
        required=False
    )

    class Meta:
        model = Profile
        fields = ['org_name', 'inn', 'personal_data', ]

    def save(self, commit=True):
        profile = super(SoleProprietorRegistrationForm, self).save(commit=False)

        profile.type = 'sole-proprietor'
        profile.activation_key = get_random_string(length=40)
        profile.key_expires = datetime.now() + timedelta(days=1)

        if commit:
            profile.save()

        return profile


class LegalPersonRegistrationForm(forms.ModelForm):
    """
    Форма регистрации для юридических лиц
    """

    LABELS = {
        'org_name':         'Наименование ЮЛ',
        'inn':              'ИНН',
        'ogrn':             'ОГРН',
        'personal_data':    'Согласие на обработку персональных данных',
    }

    HELP_TEXTS = {
        'personal_data':    'Отметив галочку вы соглашаетесь на хранение и обработку своих персональных данных',
    }

    org_name = forms.CharField(
        label=LABELS['org_name'],
        max_length=255,
        widget=forms.TextInput(),
        required=True,
    )

    inn = forms.RegexField(
        label=LABELS['inn'],
        min_length=10,
        max_length=10,
        regex=r'^[\d]{10}$',
        widget=forms.TextInput(),
        required=True
    )

    ogrn = forms.RegexField(
        label=LABELS['ogrn'],
        min_length=13,
        max_length=13,
        regex=r'^[\d]{13}$',
        widget=forms.TextInput(),
        required=True
    )

    personal_data = forms.BooleanField(
        label=LABELS['personal_data'],
        help_text=HELP_TEXTS['personal_data'],
        validators=[PersonalDataValidator.validate, ],
        required=False
    )

    class Meta:
        model = Profile
        fields = ['org_name', 'inn', 'ogrn', 'personal_data', ]

    def save(self, commit=True):
        profile = super(LegalPersonRegistrationForm, self).save(commit=False)

        profile.type = 'legal-person'
        profile.activation_key = get_random_string(length=40)
        profile.key_expires = datetime.now() + timedelta(days=1)

        if commit:
            profile.save()

        return profile


REGISTRATION_PROFILE_FORMS = {
    'individual-person': IndividualPersonRegistrationForm,
    'sole-proprietor': SoleProprietorRegistrationForm,
    'legal-person': LegalPersonRegistrationForm,
}


class ResetPasswordForm(forms.Form):
    """
    Форма для сброса пароля
    """

    LABELS = {
        'email': 'E-mail',
    }

    HELP_TEXTS = {
        'email': 'Адрес электронной почты используемый при регистрации',
    }

    email = forms.EmailField(
        label=LABELS['email'],
        help_text=HELP_TEXTS['email'],
        widget=forms.EmailInput(),
        required=True
    )

    def clean_email(self):

        email = self.cleaned_data.get('email')

        user = get_user_model()

        if not user.objects.filter(email=email).count():
            raise forms.ValidationError('Такой почтовый адрес не зарегистрирован')

        if user.is_superuser():
            raise forms.ValidationError('Данный адрес используется администраторами')

        if not Profile.objects.filter(user__email=email):
            raise forms.ValidationError('Пользователи без профиля не могут сбрасывать пароль')

        return email

    def save(self):

        email = self.cleaned_data.get('email')

        profile = Profile.objects.get(user__email=email)

        profile.activation_key = get_random_string(length=40)
        profile.key_expires = datetime.now() + timedelta(days=1)
        profile.save()

        return {
            'email': email,
            'key': profile.activation_key,
            'expires': profile.key_expires,
        }


class NewPasswordForm(SetPasswordForm):
    """
    Форма для установки нового пароля
    """

    def __init__(self, *args, **kwargs):
        super(NewPasswordForm, self).__init__(*args, **kwargs)
        self.fields['new_password2'].help_text = \
            'Для подтверждения введите, пожалуйста, пароль ещё раз.'


class ChangePasswordForm(PasswordChangeForm):
    """
    Форма смены паролей
    """

    def __init__(self, *args, **kwargs):
        super(ChangePasswordForm, self).__init__(*args, **kwargs)
        self.fields['new_password2'].help_text = \
            'Для подтверждения введите, пожалуйста, пароль ещё раз.'
