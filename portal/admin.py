# -*- coding: utf-8 -*-
"""
Copyright 2017 Dmitriy Vasilyev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
from __future__ import unicode_literals
from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.admin import GroupAdmin

from .models import Articles, User, Profile, ProxyGroup


class UserView:
    """
    Класс для расширения моделей админки
    """

    user_properties = ['first_name', 'last_name', 'middle_name', 'email', ]
    profile_properties = ['type', 'org_name', 'inn', 'ogrn', ]

    fields = []

    @classmethod
    def generate_attributes(cls):

        for field in cls.user_properties:
            attribute_name = '_user_' + field
            setattr(cls, attribute_name, cls.__user_attribute(field))

            if '__func__' in dir(getattr(cls, attribute_name)):
                getattr(cls, attribute_name).__func__.short_description = User.get_verbose_name(field)
            else:
                getattr(cls, attribute_name).short_description = User.get_verbose_name(field)

            yield attribute_name

        for field in cls.profile_properties:
            attribute_name = '_profile_' + field
            setattr(cls, attribute_name, cls.__profile_attribute(field))

            if '__func__' in dir(getattr(cls, attribute_name)):
                getattr(cls, attribute_name).__func__.short_description = Profile.LABELS[field]
            else:
                getattr(cls, attribute_name).short_description = Profile.LABELS[field]

            yield attribute_name

    @staticmethod
    def __user_attribute(user_property):
        display_method = 'get_' + user_property + '_display'
        return lambda cls, i, x=display_method, y=user_property: \
            getattr(i.user, x)() if x in dir(i.user) else getattr(i.user, y)

    @staticmethod
    def __profile_attribute(profile_property):
        display_method = 'get_' + profile_property + '_display'
        return lambda cls, i, x=display_method, y=profile_property: \
            getattr(i.user.profile, x)() if x in dir(i.user.profile) else getattr(i.user.profile, y)


UserView.fields = [item for item in UserView.generate_attributes()]


class ArticlesAdmin(admin.ModelAdmin):
    """
    Статьи
    """
    fields = ['title', 'text', ]


class ProfileAdminInline(admin.StackedInline):
    """
    Профиль пользователя
    """
    model = Profile
    fields = ['user', 'type', 'org_name', 'inn', 'ogrn', ]


class UserAdmin(BaseUserAdmin):
    """
    Пользователь
    """
    fieldsets = BaseUserAdmin.fieldsets
    fieldsets[1][1]['fields'] = ('first_name', 'last_name', 'middle_name', 'email', )
    inlines = [ProfileAdminInline, ]


admin.site.unregister(Group)
admin.site.register(Articles, ArticlesAdmin)
admin.site.register(User, UserAdmin)
admin.site.register(ProxyGroup, GroupAdmin)
