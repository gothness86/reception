# -*- coding: utf-8 -*-
"""
Copyright 2017 Dmitriy Vasilyev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
from __future__ import unicode_literals
import os
import sys
import codecs

from django.core.wsgi import get_wsgi_application

BASE_DIR = os.path.dirname(os.path.dirname(
    os.path.abspath(__file__.decode(sys.getfilesystemencoding()))
))

sys.path.insert(0, BASE_DIR)

try:
    with codecs.open(os.path.join(BASE_DIR, '.venv'), encoding='utf-8') as fd:
        for line in fd:
            sys.path.insert(0, line.strip())
except OSError:
    raise SystemError("Environment file not found")

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "reception.settings")

application = get_wsgi_application()
