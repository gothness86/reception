# -*- coding: utf-8 -*-
"""
Copyright 2017 Dmitriy Vasilyev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
from __future__ import unicode_literals
import os
import json
import codecs
import sys

SETTINGS_ROOT = os.path.dirname(
    os.path.abspath(__file__.decode(sys.getfilesystemencoding()))
)

PROJECT_ROOT = os.path.dirname(SETTINGS_ROOT)
BASE_DIR = os.path.dirname(PROJECT_ROOT)

try:
    with codecs.open(os.path.join(BASE_DIR, 'settings.json'), encoding='utf-8') as fd:
        JSON_SETTINGS = json.load(fd)
except OSError:
    raise SystemError("Settings file not found")

SECRET_KEY = JSON_SETTINGS.get('secret_key', 'defaultSuperKey#!~QQ')

DEBUG = bool(os.environ.get('DJANGO_DEBUG', ''))

ROOT_URLCONF = 'reception.urls'

WSGI_APPLICATION = 'reception.wsgi.application'

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

ALLOWED_HOSTS = ['*']

