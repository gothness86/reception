# -*- coding: utf-8 -*-
"""
Copyright 2017 Dmitriy Vasilyev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
from __future__ import unicode_literals
import os

from .main import BASE_DIR

STATEMENT_TYPES = {
    'technological-connection': 'Осуществление технологического присоединения',
    'energy-transmission': 'Оказание услуг по передаче электрической энергии',
    'energy-accounting': 'Организация учета электрической энергии',
    'customer-service': 'Обслуживание потребителей',
    'contact-information': 'Контактная информация сетевой организации',
    'quality': 'Предоставление информации о причинах несоблюдения требований к параметрам качества электроэнергии',
    'complaint': 'Прием жалобы',
    'report': 'Прием сообщения о бездоговорном потреблении электрической энергии',
}

STATEMENT_VALID_DAYS = {
    'technological-connection': 15,
    'energy-transmission': 15,
    'energy-accounting': 15,
    'customer-service': 15,
    'contact-information': 15,
    'quality': 15,
    'complaint': 30,
    'report': 1,
}

STATEMENT_COM_METHOD = (
    ('email', 'Электронная почта'),
    ('phone', 'Телефон'),
    ('mailbox', 'Почтовое отправление'),
    ('self', 'Лично (Через представителя)'),
)

STATEMENT_COM_METHOD_REQ_FIELDS = (
    ('email', 'email'),
    ('mailbox', 'post_address'),
)

STATEMENT_STATUSES = (
    (0, 'Аннулировано'),
    (1, 'Принято в обработку'),
    (2, 'Выполнено'),
)


STATEMENT_FILES_ROOT = os.path.join(BASE_DIR, 'documents/')
STATEMENT_FILES_URL = '/statements/attachment/'
STATEMENT_FILES_MAX_SIZE = 10 * (1024 ** 2)     # 10 Mb
STATEMENT_FILES_VALID_EXTENSIONS = (
    'zip', 'rar', '7z',
    'jpeg', 'jpg', 'tiff',
    'pdf', 'doc', 'xls', 'docx', 'xlsx'
)
