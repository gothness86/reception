# -*- coding: utf-8 -*-
"""
Copyright 2017 Dmitriy Vasilyev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
from __future__ import unicode_literals

from .main import DEBUG, JSON_SETTINGS

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
EMAIL_SUBJECT_PREFIX = '[МУП "АЭСК"] '

SERVER_EMAIL = 'noreply@aesk51.ru'
DEFAULT_FROM_EMAIL = 'Интернет-приёмная МУП "АЭСК" <%s>' % SERVER_EMAIL

if not DEBUG:
    EMAIL_HOST = JSON_SETTINGS.get('email_host', '')
    EMAIL_PORT = 465
    EMAIL_HOST_USER = JSON_SETTINGS.get('email_user', '')
    EMAIL_HOST_PASSWORD = JSON_SETTINGS.get('email_password', '')
    EMAIL_USE_SSL = True
    EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
