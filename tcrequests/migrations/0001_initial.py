# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-03-31 09:56
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import tcrequests.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='TCRequestFiles',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=75, verbose_name='\u0418\u043c\u044f \u0444\u0430\u0439\u043b\u0430')),
                ('file', models.FileField(storage=tcrequests.models.TCRequestsFilesStorage(), upload_to=b'', verbose_name='\u0424\u0430\u0439\u043b')),
                ('date', models.DateTimeField(auto_now=True, verbose_name='\u0414\u0430\u0442\u0430')),
                ('is_request', models.BooleanField(choices=[(0, '\u041d\u0435\u0442'), (1, '\u0414\u0430')], default=False, verbose_name='\u0421\u043e\u0434\u0435\u0440\u0436\u0438\u0442 \u0437\u0430\u044f\u0432\u043a\u0443?')),
            ],
            options={
                'db_table': 'tcrequest_files',
                'verbose_name': '\u041f\u0440\u0438\u043a\u0440\u0435\u043f\u043b\u0451\u043d\u043d\u044b\u0435 \u0444\u0430\u0439\u043b\u044b',
                'verbose_name_plural': '\u041f\u0440\u0438\u043a\u0440\u0435\u043f\u043b\u0451\u043d\u043d\u044b\u0435 \u0444\u0430\u0439\u043b\u044b',
            },
        ),
        migrations.CreateModel(
            name='TCRequests',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.IntegerField(choices=[(0, '\u0410\u043d\u043d\u0443\u043b\u0438\u0440\u043e\u0432\u0430\u043d\u0430'), (1, '\u041f\u0440\u0438\u043d\u044f\u0442\u0430 \u0432 \u043e\u0431\u0440\u0430\u0431\u043e\u0442\u043a\u0443'), (2, '\u0414\u043e\u043e\u0444\u043e\u0440\u043c\u043b\u0435\u043d\u0438\u0435 \u0434\u043e\u043a\u0443\u043c\u0435\u043d\u0442\u043e\u0432'), (3, '\u041f\u043e\u0434\u0433\u043e\u0442\u043e\u0432\u043a\u0430 \u043f\u0440\u043e\u0435\u043a\u0442\u0430 \u0434\u043e\u0433\u043e\u0432\u043e\u0440\u0430'), (5, '\u0412\u044b\u043f\u043e\u043b\u043d\u0435\u043d\u0430')], default=1, verbose_name='\u0421\u043e\u0441\u0442\u043e\u044f\u043d\u0438\u0435')),
                ('date', models.DateTimeField(auto_now=True, verbose_name='\u0414\u0430\u0442\u0430')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='\u041f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044c')),
            ],
            options={
                'db_table': 'tcrequests',
                'verbose_name': '\u0417\u0430\u044f\u0432\u043a\u0438 \u043d\u0430 \u0442\u0435\u0445. \u043f\u0440\u0438\u0441\u043e\u0435\u0434\u0438\u043d\u0435\u043d\u0438\u0435',
                'verbose_name_plural': '\u0417\u0430\u044f\u0432\u043a\u0438 \u043d\u0430 \u0442\u0435\u0445. \u043f\u0440\u0438\u0441\u043e\u0435\u0434\u0438\u043d\u0435\u043d\u0438\u0435',
                'permissions': (('notify', '\u041f\u043e\u043b\u0443\u0447\u0430\u0442\u044c \u0443\u0432\u0435\u0434\u043e\u043c\u043b\u0435\u043d\u0438\u044f'),),
            },
        ),
        migrations.CreateModel(
            name='TCRequestsInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('text', models.TextField(verbose_name='\u0422\u0435\u043a\u0441\u0442')),
                ('state', models.BooleanField(choices=[(0, '\u0417\u0430\u043a\u0440\u044b\u0442\u044c \u043f\u043e \u0443\u043c\u043e\u043b\u0447\u0430\u043d\u0438\u044e'), (1, '\u0420\u0430\u0441\u043a\u0440\u044b\u0442\u044c \u043f\u043e \u0443\u043c\u043e\u043b\u0447\u0430\u043d\u0438\u044e')], default=False, verbose_name='\u0421\u043e\u0441\u0442\u043e\u044f\u043d\u0438\u0435')),
            ],
            options={
                'db_table': 'tcrequests_info',
                'verbose_name': '\u0414\u043e\u043f. \u0438\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u044f',
                'verbose_name_plural': '\u0414\u043e\u043f. \u0438\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u044f',
            },
        ),
        migrations.AddField(
            model_name='tcrequestfiles',
            name='tcrequest',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tcrequests.TCRequests', verbose_name='\u0417\u0430\u044f\u0432\u043a\u0430'),
        ),
    ]
