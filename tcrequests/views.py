# -*- coding: utf-8 -*-
"""
Copyright 2017 Dmitriy Vasilyev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
from __future__ import unicode_literals
from django.conf import settings
from django.shortcuts import render, redirect
from django.http import Http404, StreamingHttpResponse
from django.core.mail import EmailMessage
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.template.loader import get_template
from django.db.models import Q
from django.utils.encoding import iri_to_uri
from django.utils.http import urlquote


from wsgiref.util import FileWrapper


from .models import TCRequests, TCRequestFiles, TCRequestsInfo, TCRequestsForm


@login_required()
def add_tcrequest(request):
    """
    Подача заявки на технологическое присоединение
    """

    title = 'Подача заявки на технологическое присоединение'

    if request.POST:

        form = TCRequestsForm(
            user=request.user,
            data=request.POST,
            files=request.FILES
        )

        if form.is_valid():

            tcrequest = form.save()

            message = get_template('tcrequests/notify.txt')

            url = ('https://' if request.is_secure() else 'http://') + request.get_host()

            context = {
                'url': url,
                'data': tcrequest,
                'files': TCRequestFiles.objects.filter(tcrequest=tcrequest),
            }

            email = EmailMessage(
                subject='%s Заявка на технологическое присоединение' % settings.EMAIL_SUBJECT_PREFIX,
                body=message.render(context),
                to=[user.email for user in TCRequests.notify_users() if user.email],
            )

            email.send()

            messages.success(request, 'Заявка на технологическое присоединение № #%06d зарегистрирована' % tcrequest.pk)
            return redirect(tcrequests_list)
    else:
        form = TCRequestsForm(request.user)

    info = TCRequestsInfo.objects.all().order_by('pk')

    return render(request, 'tcrequests/add_tcrequest.html', {
        'title': title,
        'forms': (form, ),
        'info': info,
        'submit': 'Отправить заявку',
    })


@login_required()
def tcrequests_list(request):
    """
    Отображение списка заявок
    """

    title = 'Заявки на технологическое присоединение'

    paginator = Paginator(
        TCRequests.objects.filter(
            user=request.user
        ).order_by('-id'), 30)

    if 'page' in request.GET:
        try:
            items = paginator.page(request.GET.get('page'))
        except PageNotAnInteger:
            items = paginator.page(1)
        except EmptyPage:
            items = paginator.page(paginator.num_pages)
    else:
        items = paginator.page(1)

    return render(request, 'tcrequests/tcrequests_list.html', {
        'title': title,
        'items': items,
    })


@login_required()
def show_tcrequest(request):
    """
    Получение подробной информации о заявлении и обращении
    """

    if 'id' not in request.GET:
        raise Http404

    try:
        data = TCRequests.objects.get(
            user=request.user,
            pk=request.GET.get('id')
        )
    except TCRequests.DoesNotExist:
        raise Http404

    files = TCRequestFiles.objects.filter(tcrequest=data)

    return render(request, 'tcrequests/show_tcrequest.html', {
        'data': data,
        'files': files,
    })


@login_required()
def tcrequest_attachment(request, filename):
    """
    Загрузка прикреплённых файлов
    """

    try:
        if not request.user.is_staff:
            tcfiles = TCRequestFiles.objects.get(
                Q(file=filename) | Q(file='./' + filename),
                tcrequest__user=request.user
            )
        else:
            tcfiles = TCRequestFiles.objects.get(
                Q(file=filename) | Q(file='./' + filename)
            )

    except TCRequestFiles.DoesNotExist:
        raise Http404

    response = StreamingHttpResponse(
        streaming_content=FileWrapper(tcfiles.file),
        content_type='application/octet-stream'
    )

    response['Content-Disposition'] = \
        'attachment; filename*=UTF-8\'\'%s;' % (
            iri_to_uri(urlquote(tcfiles.name))
        )

    return response

