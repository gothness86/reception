# -*- coding: utf-8 -*-
"""
Copyright 2017 Dmitriy Vasilyev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
from __future__ import unicode_literals
from django import forms
from django.conf import settings
from django.db import models
from django.db.models import Q
from django.core.files.storage import FileSystemStorage
from django.core.files.uploadedfile import UploadedFile
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from django.utils.text import get_valid_filename
from django.utils.crypto import get_random_string
from django.utils.encoding import python_2_unicode_compatible

import re


class TCRequestsFilesStorage(FileSystemStorage):
    """
    Хранилище
    """

    def __init__(self):
        super(TCRequestsFilesStorage, self).__init__(
            location=settings.TCREQUEST_FILES_ROOT,
            base_url=settings.TCREQUEST_FILES_URL
        )

    def get_valid_name(self, name):
        return get_valid_filename(get_random_string(length=25))


@python_2_unicode_compatible
class TCRequests(models.Model):
    """
    Заявки на тех. присоединение
    """

    LABELS = {
        'user':     'Пользователь',
        'status':   'Состояние',
        'date':     'Дата',
    }

    class Meta:
        db_table = 'tcrequests'
        verbose_name = 'Заявки на тех. присоединение'
        verbose_name_plural = 'Заявки на тех. присоединение'
        permissions = (
            ('notify', 'Получать уведомления'),
        )

    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=LABELS['user'])

    status = models.IntegerField(
        default=1,
        choices=settings.TCREQUEST_STATUSES,
        verbose_name=LABELS['status'])

    date = models.DateTimeField(auto_now=True, verbose_name=LABELS['date'])

    @staticmethod
    def notify_users():
        """
        Список пользователей для уведомления
        """

        permission = Permission.objects.get(
            codename='notify',
            content_type__app_label='tcrequests'
        )

        user = get_user_model()
        return user.objects.filter(
            Q(groups__permissions=permission) |
            Q(user_permissions=permission)
        ).distinct()

    def __str__(self):
        return '#%06d' % self.pk


class TCRequestFiles(models.Model):
    """
    Дополнительные файлы для тех. присоединения
    """

    LABELS = {
        'tcrequest':    'Заявка',
        'name':         'Имя файла',
        'file':         'Файл',
        'date':         'Дата',
        'is_request':   'Содержит заявку?',
    }

    IS_REQUEST = (
        (0, 'Нет'),
        (1, 'Да'),
    )

    class Meta:
        db_table = 'tcrequest_files'
        verbose_name = 'Прикреплённые файлы'
        verbose_name_plural = 'Прикреплённые файлы'

    tcrequest = models.ForeignKey(TCRequests, verbose_name=LABELS['tcrequest'])
    name = models.CharField(max_length=75, verbose_name=LABELS['name'])

    file = models.FileField(
        storage=TCRequestsFilesStorage(),
        max_length=100,
        verbose_name=LABELS['file']
    )

    date = models.DateTimeField(auto_now=True, verbose_name=LABELS['date'])

    is_request = models.BooleanField(
        choices=IS_REQUEST,
        default=False,
        verbose_name=LABELS['is_request']
    )


@python_2_unicode_compatible
class TCRequestsInfo(models.Model):
    """
    Условия для осуществления технологического присоединения
    """

    LABELS = {
        'title':    'Заголовок',
        'text':     'Текст',
        'state':    'Состояние',
    }

    STATES = (
        (0, 'Закрыть по умолчанию'),
        (1, 'Раскрыть по умолчанию'),
    )

    class Meta:
        db_table = 'tcrequests_info'
        verbose_name = 'Доп. информация'
        verbose_name_plural = 'Доп. информация'

    title = models.CharField(max_length=255, verbose_name=LABELS['title'])
    text = models.TextField(verbose_name=LABELS['text'])
    state = models.BooleanField(choices=STATES, default=False, verbose_name=LABELS['state'])

    def __str__(self):
        return self.title


class TCRequestsForm(forms.ModelForm):
    """
    Форма отправки заявки на технологическое присоединение
    """

    LABELS = {
        'request':  'Заявка',
        'additional': 'Доп. документы'
    }

    HELP_TEXTS = {
        'request':  'Файл содержащий ТОЛЬКО заявку на технологическое присоединение',
        'additional': 'Дополнительные документы к заявке, при необходимости можно упаковать в архив ZIP, RAR, 7Z',
    }

    EXTENSION = re.compile(r'\.(?P<ext>[\w]{1,10})$')

    class Meta:
        model = TCRequests
        fields = ['request', 'additional', ]

    request = forms.FileField(
        label=LABELS['request'],
        help_text=HELP_TEXTS['request'],
        max_length=75,
        widget=forms.FileInput(),
        required=True
    )

    additional = forms.FileField(
        label=LABELS['additional'],
        help_text=HELP_TEXTS['additional'],
        max_length=75,
        widget=forms.FileInput(),
        required=False
    )

    def __init__(self, user, *args, **kwargs):
        super(TCRequestsForm, self).__init__(*args, **kwargs)
        self.user = user

    def clean(self):

        cleaned_data = super(TCRequestsForm, self).clean()

        for field in ('request', 'additional', ):

            file = cleaned_data.get(field)

            if isinstance(file, UploadedFile):

                errors = []

                if file.size > settings.TCREQUEST_FILES_MAX_SIZE:
                    errors.append('Размер файла %s превышает допустимое значение' % file.name)

                search = self.EXTENSION.search(file.name)

                if not search:
                    errors.append('Неизвестное расширение файла')
                else:
                    if search.group('ext').lower() not in settings.TCREQUEST_FILES_VALID_EXTENSIONS:
                        errors.append('Недопустимое расширение файла %s' % file.name)

                if len(errors):
                    self._errors[field] = self.error_class(errors)
                    del cleaned_data[field]

        return cleaned_data

    def save(self, commit=True):
        model = super(TCRequestsForm, self).save(commit=False)
        model.user = self.user

        if commit:
            model.save()

            for field in ('request', 'additional', ):
                file = self.cleaned_data.get(field)
                if isinstance(file, UploadedFile):
                    TCRequestFiles(
                        tcrequest=model,
                        name=file.name,
                        file=file,
                        is_request=(True if field == 'request' else False)
                    ).save()

        return model
