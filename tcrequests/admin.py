# -*- coding: utf-8 -*-
"""
Copyright 2017 Dmitriy Vasilyev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
from __future__ import unicode_literals
from django.contrib import admin

from portal.admin import UserView
from .models import TCRequests, TCRequestFiles, TCRequestsInfo


class TCRequestFilesAdmin(admin.ModelAdmin):
    """
    Файлы заявок на технологическое присоединение
    """

    fields = ['name', 'tcrequest', 'file', 'date', ]
    readonly_fields = ['date', ]
    list_display = ['name', 'date', ]


class TCRequestFilesAdminInline(admin.TabularInline):
    """
    Файлы заявок на технологическое присоединение
    """

    model = TCRequestFiles
    fields = ['name', 'file', 'is_request', 'date', ]
    readonly_fields = ['date', ]


class TCRequestsAdmin(admin.ModelAdmin, UserView):
    """
    Заявка на технологическое присоединение
    """

    fieldsets = (
        ('Заявка на тех. присоединение', {
            'fields': [
                'user', 'status', 'date',
            ]
        }),
    )

    user_fields = ('Пользователь', {'fields': UserView.fields})

    readonly_fields = UserView.fields + ['date', ]
    list_display = ['__str__', 'date', ]
    inlines = [TCRequestFilesAdminInline, ]

    def get_fieldsets(self, request, obj=None):
        fieldsets = super(TCRequestsAdmin, self).get_fieldsets(request, obj)

        if obj is not None:
            fieldsets = [self.user_fields, fieldsets[0]]

        return fieldsets

    def get_readonly_fields(self, request, obj=None):
        fields = super(TCRequestsAdmin, self).get_readonly_fields(request, obj)

        if obj is not None:
            fields.append('user')
            fields.extend(UserView.fields)

        return fields


class TCRequestsInfoAdmin(admin.ModelAdmin):
    """
    Информация необходимая для подачи заявки
    """

    fields = ['title', 'text', 'state', ]
    list_display = ['__str__', 'state', ]


admin.site.register(TCRequestFiles, TCRequestFilesAdmin)
admin.site.register(TCRequests, TCRequestsAdmin)
admin.site.register(TCRequestsInfo, TCRequestsInfoAdmin)


