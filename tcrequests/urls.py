# -*- coding: utf-8 -*-
"""
Copyright 2017 Dmitriy Vasilyev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
from __future__ import unicode_literals
from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^add/$', views.add_tcrequest, name='add_tcrequest'),
    url(r'^list/$', views.tcrequests_list, name='tcrequests_list'),
    url(r'^show/$', views.show_tcrequest, name='show_tcrequest'),
    url(r'^attachment/([\d\S]+)/$', views.tcrequest_attachment, name='tcrequest_attachment')
]
