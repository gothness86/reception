#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os
import sys

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "reception.settings")
    from django.core.management import execute_from_command_line
    params = map(lambda arg: arg.decode(sys.stdout.encoding), sys.argv)
    execute_from_command_line(params)
