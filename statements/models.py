# -*- coding: utf-8 -*-
"""
Copyright 2017 Dmitriy Vasilyev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
from __future__ import unicode_literals
from django import forms
from django.conf import settings
from django.db import models
from django.db.models import Q
from django.core.files.storage import FileSystemStorage
from django.core.files.uploadedfile import UploadedFile
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from django.utils.text import get_valid_filename
from django.utils.crypto import get_random_string
from django.utils.encoding import python_2_unicode_compatible

import re


@python_2_unicode_compatible
class Statements(models.Model):
    """
    Обращения и заявления
    """

    LABELS = {
        'user':             'Пользователь',
        'type':             'Тип',
        'com_method':       'Способ ответа',
        'email':            'E-mail',
        'representative':   'Представит.',
        'post_address':     'Почтовый адрес',
        'phone':            'Телефон',
        'text':             'Текст обращения',
        'status':           'Статус',
        'date':             'Дата',
    }

    STATEMENTS = [(name, value) for name, value in settings.STATEMENT_TYPES.items()]
    STATEMENTS.sort(key=lambda x: x[0])

    class Meta:
        db_table = 'statements'
        verbose_name = 'Заявления и обращения'
        verbose_name_plural = 'Заявления и обращения'
        permissions = (
            ('notify', 'Получать уведомления'),
        )

    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=LABELS['user'])
    type = models.CharField(max_length=30, choices=STATEMENTS, verbose_name=LABELS['type'])

    com_method = models.CharField(
        max_length=10,
        choices=settings.STATEMENT_COM_METHOD,
        verbose_name=LABELS['com_method']
    )

    email = models.EmailField(null=True, blank=True, verbose_name=LABELS['email'])
    representative = models.CharField(max_length=255, null=True, blank=True, verbose_name=LABELS['representative'])
    post_address = models.CharField(max_length=255, null=True, blank=True, verbose_name=LABELS['post_address'])
    phone = models.CharField(max_length=30, verbose_name=LABELS['phone'])
    text = models.TextField(verbose_name=LABELS['text'])
    status = models.IntegerField(default=1, choices=settings.STATEMENT_STATUSES, verbose_name=LABELS['status'])
    date = models.DateTimeField(auto_now=True, verbose_name=LABELS['date'])

    @staticmethod
    def notify_users():
        """
        Список пользователей для уведомления
        """

        permission = Permission.objects.get(
            codename='notify',
            content_type__app_label='statements'
        )

        user = get_user_model()
        return user.objects.filter(
            Q(groups__permissions=permission) |
            Q(user_permissions=permission)
        ).distinct()

    def __str__(self):
        return "#%06d" % self.pk


class StatementFilesStorage(FileSystemStorage):
    """
    Хранилище
    """

    def __init__(self):
        super(StatementFilesStorage, self).__init__(
            location=settings.STATEMENT_FILES_ROOT,
            base_url=settings.STATEMENT_FILES_URL
        )

    def get_valid_name(self, name):
        return get_valid_filename(get_random_string(length=25))

    def url(self, name):
        result = super(StatementFilesStorage, self).url(name)

        return result


class StatementFiles(models.Model):
    """
    Файлы обращений, заявлений
    """

    LABELS = {
        'statement':    'Заявка',
        'name':         'Имя файла',
        'file':         'Файл',
        'date':         'Дата',
    }

    class Meta:
        db_table = 'statement_files'
        verbose_name = 'Прикреплённые файлы'
        verbose_name_plural = 'Прикреплённые файлы'

    statement = models.ForeignKey(Statements, verbose_name=LABELS['statement'])
    name = models.CharField(max_length=75, verbose_name=LABELS['name'])

    file = models.FileField(
        storage=StatementFilesStorage(),
        max_length=100,
        verbose_name=LABELS['file'])

    date = models.DateTimeField(auto_now=True, verbose_name=LABELS['date'])


class StatementsForm(forms.ModelForm):
    """
    Форма для подачи обращения или заявления
    """

    LABELS = {
        'days':             'Срок рассмотрения',
        'com_method':       'Способ ответа',
        'email':            'E-mail',
        'representative':   'Ф.И.О представителя',
        'post_address':     'Почтовый адрес',
        'phone':            'Телефон',
        'text':             'Текст обращения',
        'file':             'Материалы',
    }

    HELP_TEXTS = {
        'days':             'Срок рассмотрения в днях',
        'email':            'Введите адрес электронной почты',
        'representative':   'Указывается в том случае, когда заявитель не может получить ответ лично',
        'post_address':     'Укажите индекс и точный почтовый адрес',
        'file':             'Выберите файл, чтобы приложить к заявлению или обращению',
    }

    EXTENSION = re.compile(r'\.(?P<ext>[\w]{1,10})$')

    class Meta:
        model = Statements
        fields = ['days', 'com_method', 'email', 'representative', 'post_address', 'phone', 'text', 'file', ]

    days = forms.IntegerField(
        label=LABELS['days'],
        help_text=HELP_TEXTS['days'],
        widget=forms.TextInput(attrs={
            'readonly': True,
        }),
        required=True
    )

    com_method = forms.ChoiceField(
        label=LABELS['com_method'],
        choices=settings.STATEMENT_COM_METHOD,
        widget=forms.Select(),
        required=True
    )

    email = forms.EmailField(
        label=LABELS['email'],
        help_text=HELP_TEXTS['email'],
        widget=forms.EmailInput(),
        required=False
    )

    representative = forms.CharField(
        label=LABELS['representative'],
        help_text=HELP_TEXTS['representative'],
        max_length=255,
        widget=forms.TextInput(),
        required=False
    )

    post_address = forms.CharField(
        label=LABELS['post_address'],
        help_text=HELP_TEXTS['post_address'],
        max_length=255,
        widget=forms.TextInput(),
        required=False
    )

    phone = forms.CharField(
        label=LABELS['phone'],
        max_length=30,
        widget=forms.TextInput(),
        required=True
    )

    text = forms.CharField(
        label=LABELS['text'],
        widget=forms.Textarea(),
        required=True,
    )

    file = forms.FileField(
        label=LABELS['file'],
        help_text=HELP_TEXTS['file'],
        max_length=75,
        widget=forms.FileInput(),
        required=False
    )

    def __init__(self, user, statement_type, *args, **kwargs):
        super(StatementsForm, self).__init__(*args, **kwargs)
        self.user = user
        self.type = statement_type

    def clean(self):

        cleaned_data = super(StatementsForm, self).clean()

        for (method, field) in settings.STATEMENT_COM_METHOD_REQ_FIELDS:
            if cleaned_data['com_method'] == method:
                if not cleaned_data[field]:
                    self._errors[field] = self.error_class(['Обязательное поле', ])
                    del cleaned_data[field]

        file = cleaned_data.get('file')

        if isinstance(file, UploadedFile):

            errors = []

            if file.size > settings.STATEMENT_FILES_MAX_SIZE:
                errors.append('Размер файла %s превышает допустимое значение' % file.name)

            search = self.EXTENSION.search(file.name)

            if not search:
                errors.append('Неизвестное расширение файла')
            else:
                if search.group('ext').lower() not in settings.STATEMENT_FILES_VALID_EXTENSIONS:
                    errors.append('Недопустимое расширение файла %s' % file.name)

            if len(errors):
                self._errors['file'] = self.error_class(errors)
                del cleaned_data['file']

        return cleaned_data

    def save(self, commit=True):
        model = super(StatementsForm, self).save(commit=False)
        model.user = self.user
        model.type = self.type

        if commit:
            model.save()

            file = self.cleaned_data.get('file')
            if isinstance(file, UploadedFile):
                StatementFiles(
                    statement=model,
                    name=file.name,
                    file=file
                ).save()

        return model
