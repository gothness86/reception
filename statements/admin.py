# -*- coding: utf-8 -*-
"""
Copyright 2017 Dmitriy Vasilyev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
from __future__ import unicode_literals
from django.contrib import admin

from portal.admin import UserView
from .models import Statements, StatementFiles


class StatementFilesAdmin(admin.ModelAdmin):
    """
    Файлы прикреплённые к заявлениям и обращениям
    """

    fields = ['name', 'statement', 'file', 'date', ]
    readonly_fields = ['date', ]
    list_display = ['name', 'date', ]


class StatementFilesAdminInline(admin.TabularInline):
    """
    Файлы прикреплённые к заявлениям и обращениям
    """

    model = StatementFiles
    fields = ['name', 'file', ]


class StatementsAdmin(admin.ModelAdmin, UserView):
    """
    Заявления и обращения
    """

    fieldsets = [
        ('Заявление или обращение', {
            'fields': [
                'user', 'type', 'com_method',
                'email', 'representative', 'post_address', 'phone',
                'text', 'status',
            ]
        }),
    ]

    user_fields = ('Пользователь', {'fields': UserView.fields})

    readonly_fields = []
    list_display = ['__str__', 'type', 'date', ]
    inlines = [StatementFilesAdminInline, ]

    def get_fieldsets(self, request, obj=None):
        fieldsets = super(StatementsAdmin, self).get_fieldsets(request, obj)

        if obj is not None:
            fieldsets = [self.user_fields, fieldsets[0]]

        return fieldsets

    def get_readonly_fields(self, request, obj=None):
        fields = super(StatementsAdmin, self).get_readonly_fields(request, obj)

        if obj is not None:
            fields.append('user')
            fields.extend(UserView.fields)

        return fields


admin.site.register(Statements, StatementsAdmin)
admin.site.register(StatementFiles, StatementFilesAdmin)
