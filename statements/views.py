# -*- coding: utf-8 -*-
"""
Copyright 2017 Dmitriy Vasilyev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
from __future__ import unicode_literals
from django.conf import settings
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.http import Http404, StreamingHttpResponse
from django.core.mail import EmailMessage
from django.contrib import messages
from django.template.loader import get_template
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from django.utils.encoding import iri_to_uri
from django.utils.http import urlquote


from wsgiref.util import FileWrapper


from .models import Statements, StatementFiles, StatementsForm


@login_required()
def add_statement(request, statement_type):
    """
    Создание заявления или обращения
    """

    if statement_type not in settings.STATEMENT_TYPES:
        raise Http404

    if request.POST:

        form = StatementsForm(
            user=request.user,
            data=request.POST,
            files=request.FILES,
            statement_type=statement_type
        )

        if form.is_valid():

            statement = form.save()

            message = get_template('statements/notify.txt')

            url = ('https://' if request.is_secure() else 'http://') + request.get_host()

            context = {
                'url': url,
                'data': statement,
                'files': StatementFiles.objects.filter(statement=statement)
            }

            email = EmailMessage(
                subject='%s Новое заявление или обращение' % settings.EMAIL_SUBJECT_PREFIX,
                body=message.render(context),
                to=[user.email for user in Statements.notify_users() if user.email],
            )

            email.send()

            messages.success(request, 'Заявление или обращение № #%06d зарегистрировано' % statement.pk)
            return redirect(statements_list)
    else:
        form = StatementsForm(
            user=request.user,
            statement_type=statement_type,
            initial={
                'days': settings.STATEMENT_VALID_DAYS[statement_type],
                'email': request.user.email,
            })

    return render(request, 'statements/add_statement.html', {
        'title': settings.STATEMENT_TYPES[statement_type],
        'forms': (form, ),
        'submit': 'Отправить',
    })


@login_required()
def statements_list(request):
    """
    Отображение своих заявлений и обращений
    """

    title = 'Мои заявления и обращения'

    paginator = Paginator(
        Statements.objects.filter(
            user=request.user
        ).order_by('-id'), 30)

    if 'page' in request.GET:
        try:
            items = paginator.page(request.GET.get('page'))
        except PageNotAnInteger:
            items = paginator.page(1)
        except EmptyPage:
            items = paginator.page(paginator.num_pages)
    else:
        items = paginator.page(1)

    return render(request, 'statements/statements_list.html', {
        'title': title,
        'items': items,
    })


@login_required()
def show_statement(request):
    """
    Получение подробной информации о заявлении и обращении
    """

    if 'id' not in request.GET:
        raise Http404

    try:
        data = Statements.objects.get(
            user=request.user,
            pk=request.GET.get('id')
        )
    except Statements.DoesNotExist:
        raise Http404

    files = StatementFiles.objects.filter(statement=data)

    return render(request, 'statements/show_statement.html', {
        'data': data,
        'files': files,
    })


@login_required()
def statement_attachment(request, filename):
    """
    Загрузка прикреплённых файлов
    """

    try:
        if not request.user.is_staff:
            sfiles = StatementFiles.objects.get(
                Q(file=filename) | Q(file='./' + filename),
                statement__user=request.user
            )
        else:
            sfiles = StatementFiles.objects.get(
                Q(file=filename) | Q(file='./' + filename)
            )

    except StatementFiles.DoesNotExist:
        raise Http404

    response = StreamingHttpResponse(
        streaming_content=FileWrapper(sfiles.file),
        content_type='application/octet-stream'
    )

    response['Content-Disposition'] = \
        'attachment; filename*=UTF-8\'\'%s;' % (
            iri_to_uri(urlquote(sfiles.name))
        )

    return response
