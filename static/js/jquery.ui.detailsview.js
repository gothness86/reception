/**
 * Copyright 2017 Dmitriy Vasilyev
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function($) {

    /**
     * Окно детальной информации
     */
    var DetailsWindow = (function () {

        var instance;

        function createInstance() {

            var window = $('<div>').addClass('modal fade'),
                windowHeader = $('<div>').addClass('modal-header'),
                windowBody = $('<div>').addClass('modal-body'),
                windowFooter = $('<div>').addClass('modal-footer'),
                windowCloseButton = $('<a>').addClass('btn btn-primary').text('Закрыть');

            window.append(
                $('<div>').addClass('modal-dialog').append(
                    $('<div>').addClass('modal-content')
                        .append(windowHeader)
                        .append(windowBody)
                        .append(windowFooter.append(windowCloseButton))
                )
            );

            windowCloseButton.click(function () {
                window.modal('hide')
            });

            /* Публичные методы */
            return {

                setHeader: function (header) {
                    windowHeader.empty();
                    windowHeader.html($('<h4>').text(header));
                },

                setBody: function (body) {
                    windowBody.empty();
                    windowBody.html($(body));
                },

                show: function () {
                    window.modal("show");
                }

            };

        }

        return {

            getInstance: function () {

                if (!instance) {
                    instance = createInstance();
                }

                return instance;
            }
        };
    })();


    /**
     * Виджет
     */
    $.widget("ui.detailsView", {

        options: {
            'title': undefined,
            callback: function () { return undefined; }
        },

        _create: function() {

            var self = this,
                element = this.element,
                dialog = DetailsWindow.getInstance();

            element.click(function() {
                dialog.setBody();
                self.options.callback($(this), dialog.setBody);
                dialog.show();
            });

            dialog.setHeader(self.options.title);
        }
    });

})(jQuery);
