/**
 * Copyright 2017 Dmitriy Vasilyev
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function($) {

    $.widget("ui.extraFields", {

        options: {
            fields: undefined,
            parent: 'div.control-group',
            selectClass: 'selectpicker'
        },

        _create: function () {

            var self = this,
                options = self.options,
                element = this.element,
                value = element.val();

            for (var key in options.fields) {
                if (options.fields.hasOwnProperty(key)) {
                    options.fields[key].closest(options.parent).hide();
                }
            }

            if (options.fields.hasOwnProperty(value)) {
                options.fields[value].closest(options.parent).show();
            }

            element.change(function() {
                self._change();
            });

        },

        _change: function () {

            var self = this,
                options = self.options,
                value = this.element.val(),
                key;

            if (typeof options.fields[value] !== 'undefined') {

                for (key in options.fields) {
                    if (options.fields.hasOwnProperty(key)) {
                        options.fields[key].closest(options.parent).hide();
                    }
                }

                options.fields[value].closest(options.parent).fadeIn('slow');

            } else {
                for (key in options.fields) {
                    if (options.fields.hasOwnProperty(key)) {
                        options.fields[key].closest(options.parent).fadeOut('slow');
                    }
                }
            }

        }

    });

})(jQuery);