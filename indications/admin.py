# -*- coding: utf-8 -*-
"""
Copyright 2017 Dmitriy Vasilyev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
from __future__ import unicode_literals
from django.contrib import admin

from indications.models import Indications


class IndicationsAdmin(admin.ModelAdmin):
    """
    Показания счётчиков
    """

    fields = ['address', 'meter', 'contract', 'value', 'date', ]
    readonly_fields = ['date', ]
    list_display = ['meter', 'address', 'value', 'date', ]


admin.site.register(Indications, IndicationsAdmin)
