# -*- coding: utf-8 -*-
"""
Copyright 2017 Dmitriy Vasilyev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
from __future__ import unicode_literals
from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from django.db import models
from django.db.models import Q
from django.utils.encoding import python_2_unicode_compatible


@python_2_unicode_compatible
class Indications(models.Model):
    """
    Показания счётчиков
    """

    LABELS = {
        'address':  'Адрес',
        'meter':    'Счётчик',
        'contract': 'Договор',
        'value':    'Показание',
        'date':     'Дата',
    }

    class Meta:
        db_table = 'indications'
        verbose_name = 'Показания счётчиков'
        verbose_name_plural = 'Показания счётчиков'
        permissions = (
            ('notify', 'Получать уведомления'),
        )

    address = models.CharField(max_length=255, verbose_name=LABELS['address'])
    meter = models.CharField(max_length=20, verbose_name=LABELS['meter'])
    contract = models.CharField(max_length=20, verbose_name=LABELS['contract'])
    value = models.IntegerField(verbose_name=LABELS['value'])
    date = models.DateTimeField(auto_now=True, verbose_name=LABELS['date'])

    @staticmethod
    def notify_users():
        """
        Список пользователей для уведомления
        """

        permission = Permission.objects.get(
            codename='notify',
            content_type__app_label='indications'
        )

        user = get_user_model()
        return user.objects.filter(
            Q(groups__permissions=permission) |
            Q(user_permissions=permission)
        ).distinct()

    def __str__(self):
        return str(self.pk)


class IndicationsForm(forms.ModelForm):
    """
    Форма для передачи показаний
    """

    LABELS = {
        'address':  'Адрес объекта',
        'meter':    'Номер счётчика',
        'contract': 'Номер договора',
        'value':    'Показание счётчика',
    }

    class Meta:
        model = Indications
        fields = ['address', 'meter', 'contract', 'value', ]

    address = forms.CharField(
        label="Адрес объекта",
        max_length=255,
        widget=forms.TextInput(),
        required=True
    )

    meter = forms.RegexField(
        label="Номер счётчика",
        max_length=20,
        regex=r'^[\d]+$',
        widget=forms.TextInput(),
        required=True
    )

    contract = forms.RegexField(
        label="Номер договора",
        max_length=20,
        regex=r'^[\d]+$',
        widget=forms.TextInput(),
        required=True
    )

    value = forms.IntegerField(
        label="Показание счётчика",
        max_value=1000000,
        widget=forms.TextInput(),
        required=True,
    )

