# -*- coding: utf-8 -*-
"""
Copyright 2017 Dmitriy Vasilyev

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
from __future__ import unicode_literals
from django.conf import settings
from django.core.mail import EmailMessage
from django.shortcuts import render, redirect
from django.contrib import messages
from django.template.loader import get_template

from indications.models import Indications
from indications.models import IndicationsForm


def transfer_of_indication(request):
    """
    Передача показаний
    """

    title = 'Передача показаний эл. счетчиков юридическими лицами города Апатиты'

    if request.POST:

        form = IndicationsForm(request.POST)

        if form.is_valid():

            indication = form.save()

            message = get_template('indications/notify.txt')

            context = {
                'data': indication,
            }

            email = EmailMessage(
                subject='%s Передача показаний' % settings.EMAIL_SUBJECT_PREFIX,
                body=message.render(context),
                to=[user.email for user in Indications.notify_users() if user.email],
            )

            email.send()

            for field in ('address', 'meter', 'contract'):
                request.session[field] = getattr(indication, field)

            messages.success(request, 'Показания переданы')
            return redirect('/')
    else:
        form = IndicationsForm(initial={
            field: request.session.get(field) for field in ('address', 'meter', 'contract')
        })

    return render(request, 'forms.html', {
        'title': title,
        'forms': (form, ),
        'submit': 'Передать',
    })

